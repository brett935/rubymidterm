require 'test_helper'

class MidtermControllerControllerTest < ActionController::TestCase
  test "should get Exam" do
    get :Exam
    assert_response :success
    assert_select "title", "8 questions"
  end

end
