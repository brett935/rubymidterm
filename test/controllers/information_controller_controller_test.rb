require 'test_helper'

class InformationControllerControllerTest < ActionController::TestCase
  test "should get Me" do
    get :Me
    assert_response :success
    assert_select "title", "Brett Napier"
  end

  test "should get Class" do
    get :Class
    assert_response :success
    assert_select "title", "CSC178"
  end

end
